# Notice

This is a fork of https://gitlab.com/opennota/widdly. This is intended for personal use and not public distribution. I am not a developer of this package and have no intention to do so either.


# Install instructions

```
go get -u gitlab.com/ibnishak/widdly
cd $GOPATH/src/gitlab.com/ibnishak
go install ./widdly
cp ./widdly/wd ~/.bin #or wherever your `echo $PATH` outputs
```

# Building index html file
Refer https://gitlab.com/ibnishak/clevernote


# Usage

- Launching server
``` bash
widdly -db myfolderbname
```

- Use `wd --help` to see the help menu for wrapper script 

# Changes from Opennota/Widdly

- Wrapper bash script
- Removed index.html(clevernote) to its own repo with build script
- Added terminal message to indicate startup of server
- Added flatfile and sqlite support from https://github.com/xarnze/widdly
- Flatfiles are served from master branch, while sqlite is served from sqlite branch
- Changed flatfiles.go to serve `.org` files rather than `.tid` files
- Sqlite branch: Resolved the storylist reset bug, changed table name to "wiki"
